class AddressBook
  attr_accessor :contact_list
  INVALID_NAME_ERROR = "InvalidNameError: Contact name must be atleast 1 character long"

  def self.parse(&block)
    book = self.new
    book.instance_eval(&block)
    book
  end

  def initialize
    @contact_list = []
  end

  def contacts
    contact_list.map(&:name)
  end

  def contact(name, &block)
    new_contact = block_given? ? Contact.new(name, &block) : Contact.new(name)
    add_contact(new_contact)
  end

  def add_contact(contact)
    raise INVALID_NAME_ERROR if contact.name.length < 1
    contact_list << contact
  end

  def find(name)
    contact_list.find {|c| c.name.downcase == name.downcase }
  end

  def method_missing(method_sym)
    self.find(method_to_string(method_sym))
  end

  def method_to_string(method_sym)
    method_sym.to_s.gsub('_', ' ')
  end
end

