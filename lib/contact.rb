class Contact
  attr_accessor :details, :name

  def initialize(name, &block)
    @details = {}
    @name = name
    instance_eval(&block) if block_given?
  end

  def method_missing(method_sym, arg = nil)
    method_name = method_sym.to_s.gsub('=', '').to_sym
    arg ? @details[method_name] = arg : @details[method_name]
  end
end
