require 'spec_helper.rb'
require 'ostruct'

describe AddressBook do
  let(:address_book) { AddressBook.new }
  describe '#contacts' do
    context 'when there are no contacts' do
      it 'should return an empty array' do
        expect(address_book.contacts).to eq([])
      end
    end

    context 'when there are contacts' do
      it 'should return an array of all the added contacts' do
        address_book.add_contact(OpenStruct.new(name: 'Jim'))
        address_book.add_contact(OpenStruct.new(name: 'Bob'))
        expect(address_book.contacts).to eq(['Jim','Bob'])
      end
    end
  end

  describe '#add_contact' do
    before do
      address_book.add_contact(OpenStruct.new(name: 'Jim'))
      address_book.add_contact(OpenStruct.new(name: 'Bob')) 
    end
    it 'should add the contacts to the contacts array' do
      expect(address_book.contacts).to eq(['Jim','Bob'])
    end

    it 'should not add an empty string' do
      expect{ address_book.add_contact(OpenStruct.new(name: '')) }.to raise_error AddressBook::INVALID_NAME_ERROR
      expect(address_book.contacts).to eq(['Jim','Bob'])
    end
  end

  describe 'finding a contact' do
    describe '#find' do
      context 'when present' do
        it 'should return a hash with the contacts information' do
          contact_block = proc { email('jim@gmail.com'); phone(1234) }
          block = proc { contact('Jim', &contact_block) }
          book = AddressBook.parse(&block)
          expect(book.find('Jim').details).to eq({phone: 1234, email: 'jim@gmail.com'})
        end
      end

      context 'when absent' do
        it 'should return a nil' do
          contact_block = proc { email('jim@gmail.com'); phone(1234) }
          block = proc { contact('Jim', &contact_block) }
          book = AddressBook.parse(&block)
          expect(book.find('Jill')).to eq(nil)
        end
      end
    end
    
    describe 'contact name as a method of finding them' do
      it 'should return the contacts details as a hash' do
        contact_block = proc { email('jim@gmail.com'); phone(1234) }
        block = proc { contact('Jim', &contact_block) }
        book = AddressBook.parse(&block)
        expect(book.jim.details).to eq({phone: 1234, email: 'jim@gmail.com'})
      end

      it 'should return the contacts details as a hash' do
        contact_block = proc { email('jim_jones@gmail.com'); phone(1234) }
        block = proc { contact('Jim Jones', &contact_block) }
        book = AddressBook.parse(&block)
        expect(book.jim_jones.details).to eq({phone: 1234, email: 'jim_jones@gmail.com'})
      end
    end

    it 'should return nil' do
      book = AddressBook.new
      expect(book.jim).to eq(nil)
    end
  end

  describe 'call the field of a contact' do
    it 'should return the value of that field' do
      contact_block = proc { email('jim@gmail.com'); phone(1234) }
      block = proc { contact('Jim', &contact_block) }
      book = AddressBook.parse(&block)
      expect(book.jim.phone).to eq(1234)
      expect(book.jim.email).to eq('jim@gmail.com')
    end
  end

  describe '#contact' do
    context 'when given a name and a block' do
      it 'should add the name to contacts and set the contacts information' do
        block = proc { phone(1234) }
        address_book.contact('Bob', &block)
        expect(address_book.contacts).to eq(['Bob'])
      end
    end

    context 'when given a name' do
      it 'should add the name to contacts' do
        address_book.contact('Bob')
        expect(address_book.contacts).to eq(['Bob'])
      end
    end
  end

  describe '.parse' do
    it 'should run its given block' do
      block = Proc.new { contact('Jim') }
      book = AddressBook.parse(&block)
      expect(book.contacts).to eq(['Jim'])
    end
  end
end
