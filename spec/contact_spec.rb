require 'spec_helper'

describe Contact do
  describe '#initialize' do 
    it 'takes contact fields and adds it to the details for the contact' do
      block = proc { address('12 abc lane'); company('Company Inc.'); phone(1234) }
      contact = Contact.new('Jim',&block)
      expect(contact.details).to eq({address: '12 abc lane', company: 'Company Inc.', phone: 1234})
      expect(contact.address).to eq('12 abc lane')
      expect(contact.company).to eq('Company Inc.')
      expect(contact.phone).to eq(1234)
     end
  end

  describe 'Setting an attribute' do
    it 'should set attributes called on any instance of contact' do
      block = proc { address('12 abc lane'); company('Company Inc.'); phone(1234) }
      contact = Contact.new('Jim',&block)
      contact.age = 30
      expect(contact.details).to eq({address: '12 abc lane', company: 'Company Inc.', phone: 1234, age: 30})
      expect(contact.age).to eq(30)
    end
  end
end